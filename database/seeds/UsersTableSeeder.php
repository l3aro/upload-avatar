<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = "Blackheart";
        $user->email = "dgbao1340@gmail.com";
        $user->password = bcrypt('123654');
        $user->avatar = "http://127.0.0.1:8000/media/user/avatar/default.png";
        $user->save();
    }
}

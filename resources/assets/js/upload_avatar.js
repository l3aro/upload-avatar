function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);

    let formData = new FormData();
    formData.append("avatar",$('#imageUpload')[0].files[0]);

    $.ajax({
        url: "/settings/pushImage",
        data: formData,
        method: "POST",
        contentType: false,
        processData: false,
        success: function(scs) {
            swal({
                title: "Success!",
                text: "Your avatar has been uploaded",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-success",
                type: "success"
            }).catch(swal.noop);
        
        },
        error: function(err) {

            swal({
                title: "Error!",
                text: "Please try again later.",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-danger",
                type: "error"
            }).catch(swal.noop);
            
        }
    });
});